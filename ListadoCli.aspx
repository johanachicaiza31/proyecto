﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ListadoCli.aspx.cs" Inherits="ListadoCli" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   <h1>GESTION DE CLIENTES</h1>
    <br />
   <asp:Panel runat="server" ID="panel1">
         <div class="row">
            <div class="col-md-12">
                <asp:Button runat="server" 
                    ID="BtnCrear" CssClass="btn btn-success"
                    Text="Nuevo Cliente"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:GridView runat="server" ID="gvClientes" class="table table-hover table-bordered">
                        <Columns>
                            <asp:TemplateField HeaderText="ACCIONES">
                                <ItemTemplate>
                                     <asp:Button runat="server" 
                                        ID="BtnLeer" CssClass="btn btn-success"
                                        Text="Ver Detalles"/>
                                    <asp:Button runat="server" 
                                        ID="BtnActualizar" CssClass="btn btn-warning"
                                        Text="Actualizar"/>
                                    <asp:Button runat="server" 
                                        ID="BtnEliminar" CssClass="btn btn-danger"
                                        Text="Eliminar"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                </asp:GridView>
            </div>
        </div>

    </asp:Panel>



</asp:Content>

